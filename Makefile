build:
	docker build --no-cache -t fibonacci-spiral-matrix -f ./Dockerfile.test .

run:
	docker run -p 4000:4000 -i fibonacci-spiral-matrix

deploy: build run

generate-doc: generate-swagger rename-swagger-json rename-swagger-yml

generate-swagger:
	swag init -g ./cmd/server.go --output docs --parseDependency

rename-swagger-json:
	mv docs/swagger.json docs/fibonacci-spiral-matrix.json

rename-swagger-yml:
	mv docs/swagger.yaml docs/fibonacci-spiral-matrix.yml

generate-mock:
	mockery --inpackage --all

backend-app:
	docker-compose -f docker-compose.yml up --build

status := $(if $(shell docker ps -a -q),ok,notok)
docker-clean:
ifeq ($(status), ok)
	@docker stop $(shell docker ps -a -q) && docker rm $(shell docker ps -a -q) && docker volume prune -f
	@echo "the stack of containers was cleaned"
else
	@echo "the stack of containers is empty"
endif