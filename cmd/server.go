package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	log "github.com/sirupsen/logrus"
	echoSwagger "github.com/swaggo/echo-swagger"
	"github.com/swaggo/swag"
	"gitlab.com/juaninsis/fibonacci-spiral-matrix/docs"
	"gitlab.com/juaninsis/fibonacci-spiral-matrix/internal/cfg"
	"gitlab.com/juaninsis/fibonacci-spiral-matrix/internal/fibonacci"
	"gitlab.com/juaninsis/fibonacci-spiral-matrix/internal/health"
	"gitlab.com/juaninsis/fibonacci-spiral-matrix/internal/middlewares"
)

const (
	fibonacciSpiralMatrix = "fibonacci-spiral-matirx"
)

var (
	conf = cfg.GetConfig()
)

func init() {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetLevel(log.Level(conf.LogLevel))
}
func main() {
	log.Info(fibonacciSpiralMatrix, "is starting...")
	BuildServer()
}

func BuildServer() {
	e := buildEcho()
	routes := e.Group("")
	routesDoc := e.Group("")
	routes.Use(middlewares.CheckAppAuthorization, middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "method=${method}, uri=${uri}, status=${status}\n",
	}), middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, echo.HeaderAuthorization},
	}))
	serveDocs(routesDoc)
	health.Serve(routes)
	fibonacci.Serve(routes, fibonacci.NewAction())
	e.Logger.Fatal(e.Start(":" + conf.OwnPort))
}

func buildEcho() *echo.Echo {
	e := echo.New()
	return e
}

// Swagger Documentation
func serveDocs(routeGroups *echo.Group) {
	docs.SwaggerInfo.InfoInstanceName = fibonacciSpiralMatrix
	swag.Register(docs.SwaggerInfo.InstanceName(), docs.SwaggerInfo)
	routeGroups.GET("/docs/*", echoSwagger.EchoWrapHandler(
		func(c *echoSwagger.Config) { c.InstanceName = fibonacciSpiralMatrix }),
	)
}
