# fibonacci-spiral-matrix

## Description

You will be building an API-like service and a frontend client to serve the famous
spiral matrix, but with an interesting twist: The numbersinside the matrix will be
those of the Fibonaccisequence, up to the number of cellsin the matrix

## Diagram

### Sequence diagram

![alt text](https://gitlab.com/juaninsis/fibonacci-spiral-matrix/-/raw/main/diagrams/fibonacci_sequence_diagram.png?raw=true)

### Architecture diagram

![alt text](https://gitlab.com/juaninsis/fibonacci-spiral-matrix/-/raw/main/diagrams/fibonacci_architecture_diagram.png?raw=true)

## Documentation

http://localhost:4000/docs/index.html