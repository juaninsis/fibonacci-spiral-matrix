package middlewares

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
	"gitlab.com/juaninsis/fibonacci-spiral-matrix/internal/cfg"
	"gitlab.com/juaninsis/fibonacci-spiral-matrix/internal/errors"
)

const (
	Authorization = "Authorization"
)

var (
	client = &http.Client{}
	conf   = cfg.GetConfig()
)

type (
	tokenInfo struct {
		IssuedTo      string `json:"issued_to"`
		Audience      string `json:"audience"`
		UserID        string `json:"user_id"`
		Scope         string `json:"scope"`
		ExpiresIn     int    `json:"expires_in"`
		Email         string `json:"email"`
		VerifiedEmail bool   `json:"verified_email"`
		AccessType    string `json:"access_type"`
	}

	AppHeaders struct {
		Authorization string
	}
)

func getAppHeadersInfo(c echo.Context) AppHeaders {
	headers := c.Request().Header

	return AppHeaders{
		Authorization: headers.Get(Authorization),
	}
}

func CheckAppAuthorization(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		appHeaders := getAppHeadersInfo(c)
		if appHeaders.Authorization == "" {
			return errors.NewHTTPForbidden("missing Authorization in headers")
		}
		token, err := getTokenInfo(strings.Replace(appHeaders.Authorization, "Bearer ", "", -1))
		if err != nil {
			return errors.NewHTTPInternalServerError(err.Error())
		}
		if token.AccessType != "online" || !token.VerifiedEmail || token.Audience != conf.Audience {
			return errors.NewHTTPForbidden("access token is wrong")
		}
		return next(c)
	}
}

func getTokenInfo(accessToken string) (*tokenInfo, error) {
	req, err := http.NewRequest(http.MethodGet, "https://www.googleapis.com/oauth2/v1/tokeninfo?access_token="+accessToken, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	var result tokenInfo
	if err = json.Unmarshal(body, &result); err != nil {
		return nil, err
	}
	return &result, nil
}
