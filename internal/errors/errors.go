package errors

import (
	"errors"
	"net/http"

	"github.com/labstack/echo/v4"
)

func New(msg string) error {
	return errors.New(msg)
}

type (
	BadRequestError struct {
		Err error
	}
)

func (e *BadRequestError) Error() string {
	return e.Err.Error()
}
func NewBadRequest(msg string) *BadRequestError {
	return &BadRequestError{New(msg)}
}

func NewHTTPBadRequest(msg string) error {
	return echo.NewHTTPError(http.StatusBadRequest, msg)
}

func NewHTTPInternalServerError(msg string) error {
	return echo.NewHTTPError(http.StatusInternalServerError, msg)
}

func NewHTTPForbidden(msg string) error {
	return echo.NewHTTPError(http.StatusForbidden, msg)
}

func GetHTTPError(err error) error {
	switch err.(type) {
	case *BadRequestError:
		return NewHTTPBadRequest(err.Error())
	default:
		return NewHTTPInternalServerError(err.Error())

	}
}
