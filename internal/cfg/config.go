package cfg

import (
	"github.com/joeshaw/envdecode"
)

var (
	config Config
)

type (
	Config struct {
		OwnURL   string `env:"OWN_URL,default=http://127.0.0.1"`
		OwnPort  string `env:"OWN_PORT,default=4000"`
		LogLevel int64  `env:"LOG_LEVEL,default=4"`
		LogPath  string `env:"LOG_PATH,default=/tmp/logs/microservice.log"`
		Audience string `env:"AUDIENCE"`
	}
)

func initCfg() {
	if err := envdecode.Decode(&config); err != nil {
		panic(err)
	}
}

func GetConfig() Config {
	initCfg()
	return config
}
