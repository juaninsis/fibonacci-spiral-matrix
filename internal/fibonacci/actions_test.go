package fibonacci

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/juaninsis/fibonacci-spiral-matrix/internal/errors"
)

func TestGenerateFibonacciSpiral(t *testing.T) {
	tests := []struct {
		name           string
		inputR, inputC int64
		output         [][]int64
		err            error
	}{
		{
			name:   "2x2 successful generate fibonacci spiral case",
			inputR: 2,
			inputC: 2,
			output: [][]int64{
				{0, 1},
				{2, 1},
			},
		},
		{
			name:   "5x1 successful generate fibonacci spiral case",
			inputR: 5,
			inputC: 1,
			output: [][]int64{
				{0},
				{1},
				{1},
				{2},
				{3},
			},
		},
		{
			name:   "5x2 successful generate fibonacci spiral case",
			inputR: 5,
			inputC: 2,
			output: [][]int64{
				{0, 1},
				{34, 1},
				{21, 2},
				{13, 3},
				{8, 5},
			},
		},
		{
			name:   "3x3 successful generate fibonacci spiral case",
			inputR: 3,
			inputC: 3,
			output: [][]int64{
				{0, 1, 1},
				{13, 21, 2},
				{8, 5, 3},
			},
		},
		{
			name:   "5x5 successful generate fibonacci spiral case",
			inputR: 5,
			inputC: 5,
			output: [][]int64{
				{0, 1, 1, 2, 3},
				{610, 987, 1597, 2584, 5},
				{377, 28657, 46368, 4181, 8},
				{233, 17711, 10946, 6765, 13},
				{144, 89, 55, 34, 21},
			},
		},
		{
			name:   "8x8 successful generate fibonacci spiral case",
			inputR: 8,
			inputC: 8,
			output: [][]int64{
				{0, 1, 1, 2, 3, 5, 8, 13},
				{196418, 317811, 514229, 832040, 1346269, 2178309, 3524578, 21},
				{121393, 2971215073, 4807526976, 7778742049, 12586269025, 20365011074, 5702887, 34},
				{75025, 1836311903, 956722026041, 1548008755920, 2504730781961, 32951280099, 9227465, 55},
				{46368, 1134903170, 591286729879, 6557470319842, 4052739537881, 53316291173, 14930352, 89},
				{28657, 701408733, 365435296162, 225851433717, 139583862445, 86267571272, 24157817, 144},
				{17711, 433494437, 267914296, 165580141, 102334155, 63245986, 39088169, 233},
				{10946, 6765, 4181, 2584, 1597, 987, 610, 377},
			},
		},
		{
			name:   "2x3 successful generate fibonacci spiral case",
			inputR: 2,
			inputC: 3,
			output: [][]int64{
				{0, 1, 1},
				{5, 3, 2},
			},
		},
		{
			name:   "3x2 successful generate fibonacci spiral case",
			inputR: 3,
			inputC: 2,
			output: [][]int64{
				{0, 1},
				{5, 1},
				{3, 2},
			},
		},
		{
			name: "invalid param error generate fibonacci spiral case ",
			err:  errors.NewBadRequest("invalid param"),
		},
	}
	a := NewAction()
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result, err := a.GenerateSpiral(context.Background(), test.inputR, test.inputC)
			if err != nil {
				assert.Equal(t, test.err, err)
				return
			}
			assert.ElementsMatch(t, test.output, result.Rows)
		})
	}
}

func TestGetFibonacciSeries(t *testing.T) {
	tests := []struct {
		name   string
		input  int64
		output []int64
	}{
		{
			name:   "successful get fibonacci series case",
			input:  6,
			output: []int64{0, 1, 1, 2, 3, 5},
		},
		{
			name:   "one element successful get fibonacci series case",
			input:  1,
			output: []int64{0},
		},
		{
			name:   "two element successful get fibonacci series case",
			input:  2,
			output: []int64{0, 1},
		},
		{
			name:   "zero element successful get fibonacci series case",
			input:  0,
			output: nil,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := getFibonacciSeries(test.input)
			assert.Equal(t, test.output, result)
		})
	}
}
