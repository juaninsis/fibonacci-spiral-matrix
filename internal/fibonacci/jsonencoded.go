package fibonacci

type (
	Response struct {
		TS   int64     `json:"ts"`
		Rows [][]int64 `json:"rows"`
	}
)
