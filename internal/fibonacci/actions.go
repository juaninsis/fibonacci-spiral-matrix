package fibonacci

import (
	"context"
	"time"

	"gitlab.com/juaninsis/fibonacci-spiral-matrix/internal/errors"
)

type (
	action struct{}

	Actioner interface {
		GenerateSpiral(context.Context, int64, int64) (*Response, error)
	}
)

func NewAction() Actioner {
	return &action{}
}

func (a *action) GenerateSpiral(ctx context.Context, rows int64, cols int64) (*Response, error) {
	if rows == 0 || cols == 0 {
		return nil, errors.NewBadRequest("invalid param")
	}

	spiral := make([][]int64, rows)
	for i := range spiral {
		spiral[i] = make([]int64, cols)
	}

	seriesLimit := rows * cols
	next(0, rows-1, cols-1, 0, 0, seriesLimit, getFibonacciSeries(seriesLimit), spiral)
	return &Response{
		Rows: spiral,
		TS:   time.Now().Unix(),
	}, nil
}

func next(i, rowLimit, colLimit, row, col, seriesLimit int64, series []int64, spiral [][]int64) {
	for j := col; j <= colLimit; j++ {
		spiral[row][j] = series[i]
		if i++; i >= seriesLimit {
			return
		}
	}
	down(i, rowLimit, colLimit, row, col, seriesLimit, series, spiral)
}

func down(i, rowLimit, colLimit, row, col, seriesLimit int64, series []int64, spiral [][]int64) {
	for j := row + 1; j <= rowLimit; j++ {
		spiral[j][colLimit] = series[i]
		if i++; i >= seriesLimit {
			return
		}
	}
	previous(i, rowLimit, colLimit-1, row+1, col, seriesLimit, series, spiral)
}

func previous(i, rowLimit, colLimit, row, col, seriesLimit int64, series []int64, spiral [][]int64) {
	for j := colLimit; j >= col; j-- {
		spiral[rowLimit][j] = series[i]
		if i++; i >= seriesLimit {
			return
		}
	}
	up(i, rowLimit-1, colLimit, row, col, seriesLimit, series, spiral)
}

func up(i, rowLimit, colLimit, row, col, seriesLimit int64, series []int64, spiral [][]int64) {
	for j := rowLimit; j >= row; j-- {
		spiral[j][col] = series[i]
		if i++; i >= seriesLimit {
			return
		}
	}
	next(i, rowLimit, colLimit, row, col+1, seriesLimit, series, spiral)
}

func getFibonacciSeries(length int64) []int64 {
	if length <= 0 {
		return nil
	}
	if length == 1 {
		return []int64{0}
	}
	acu := []int64{0, 1}
	for i := int64(2); i < length; i++ {
		acu = append(acu, acu[i-2]+acu[i-1])
	}
	return acu
}
