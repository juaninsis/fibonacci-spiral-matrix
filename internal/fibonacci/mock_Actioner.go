// Code generated by mockery v2.14.1. DO NOT EDIT.

package fibonacci

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
)

// MockActioner is an autogenerated mock type for the Actioner type
type MockActioner struct {
	mock.Mock
}

// GenerateSpiral provides a mock function with given fields: _a0, _a1, _a2
func (_m *MockActioner) GenerateSpiral(_a0 context.Context, _a1 int64, _a2 int64) (*Response, error) {
	ret := _m.Called(_a0, _a1, _a2)

	var r0 *Response
	if rf, ok := ret.Get(0).(func(context.Context, int64, int64) *Response); ok {
		r0 = rf(_a0, _a1, _a2)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*Response)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, int64, int64) error); ok {
		r1 = rf(_a0, _a1, _a2)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

type mockConstructorTestingTNewMockActioner interface {
	mock.TestingT
	Cleanup(func())
}

// NewMockActioner creates a new instance of MockActioner. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewMockActioner(t mockConstructorTestingTNewMockActioner) *MockActioner {
	mock := &MockActioner{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
