package fibonacci

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/juaninsis/fibonacci-spiral-matrix/internal/errors"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	mock "github.com/stretchr/testify/mock"
)

func TestHandlerGet(t *testing.T) {
	tests := []struct {
		name         string
		action       Actioner
		inputR       int64
		inputC       int64
		expectedCode int
		errExpected  error
	}{
		{
			name: "successful handler get case",
			action: func() *MockActioner {
				am := &MockActioner{}
				am.On("GenerateSpiral", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
				return am
			}(),
			inputR:       3,
			inputC:       3,
			expectedCode: 200,
		},
		{
			name: "action internal server error handler get case",
			action: func() *MockActioner {
				am := &MockActioner{}
				am.On("GenerateSpiral", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("something wrong"))
				return am
			}(),
			inputR:       3,
			inputC:       3,
			expectedCode: 500,
		},
		{
			name: "action bad request error handler get case",
			action: func() *MockActioner {
				am := &MockActioner{}
				am.On("GenerateSpiral", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.NewBadRequest("something wrong"))
				return am
			}(),
			inputR:       3,
			inputC:       3,
			expectedCode: 400,
		},
		{
			name: "0 rows handler get case",
			action: func() *MockActioner {
				am := &MockActioner{}
				return am
			}(),
			inputR:       0,
			inputC:       3,
			expectedCode: 400,
		},
		{
			name: "0 cols handler get case",
			action: func() *MockActioner {
				am := &MockActioner{}
				return am
			}(),
			inputR:       3,
			inputC:       0,
			expectedCode: 400,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			e := echo.New()
			g := e.Group("")
			Serve(g, test.action)
			req := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/fibonacci-spiral?rows=%d&cols=%d", test.inputR, test.inputC), nil)
			rec := httptest.NewRecorder()
			e.ServeHTTP(rec, req)
			e.ServeHTTP(rec, req)

			assert.Equal(t, test.expectedCode, rec.Code)
			mock.AssertExpectationsForObjects(t, test.action)
		})
	}
}
