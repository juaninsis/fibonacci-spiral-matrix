package fibonacci

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
	"gitlab.com/juaninsis/fibonacci-spiral-matrix/internal/errors"
)

func Serve(routeGroups *echo.Group, a Actioner) {
	routeGroups.GET("/fibonacci-spiral", handlerGet(a))
}

// Reservation godoc
// @Summary Get Fibonacci spiral.
// @Description Get Fibonacci spiral
// @Tags Fibonacci
// @Accept json
// @Produce json
// @Param rows query int true  "the fibonacci spiral rows"
// @Param cols query int true  "the fibonacci spiral cols"
// @Success 200	 {object} Response
// @Failure 400  {object} echo.HTTPError
// @Failure 500  {object} echo.HTTPError
// @Router /fibonacci-spiral [GET]
func handlerGet(a Actioner) echo.HandlerFunc {
	return func(c echo.Context) error {
		defer func() {
			if r := recover(); r != nil {
				log.Error("recovered ", r)
			}
		}()
		var (
			err  error
			logH = log.WithField("function", "handler_get")
			ctx  = c.Request().Context()
			rows int64
			cols int64
			res  *Response
		)
		if rows, err = strconv.ParseInt(c.QueryParam("rows"), 10, 64); err != nil || rows <= 0 {
			return errors.NewHTTPBadRequest("rows must be a number")
		}
		if cols, err = strconv.ParseInt(c.QueryParam("cols"), 10, 64); err != nil || cols <= 0 {
			return errors.NewHTTPBadRequest("cols must be a number")
		}

		if res, err = a.GenerateSpiral(ctx, rows, cols); err != nil {
			logH.Error(err)
			return errors.GetHTTPError(err)
		}
		return c.JSON(http.StatusOK, res)
	}
}
