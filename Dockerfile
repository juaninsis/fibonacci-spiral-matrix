FROM golang:1.19 as builder

ENV CGO_ENABLED=0

WORKDIR /app

RUN git clone https://gitlab.com/juaninsis/fibonacci-spiral-matrix.git
WORKDIR /app/fibonacci-spiral-matrix
RUN go get -d -v ./...
RUN go build -o ./build/ cmd/server.go

#############

FROM alpine:latest

COPY --from=builder /app/fibonacci-spiral-matrix/build .

CMD ["./server"]
